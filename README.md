# GerberParserApp

Приложение для парсинга характеристик из файлов формата .GBR.
Инструменты для парсинга находятся в проекте [GerberParser.Lib](./GerberParser.Lib/)

## Пример использования

Проект с консольным приложением расположен в [GerberParser](./GerberParser/)

```
using GerberParser.Lib;
using GerberParser.Lib.GerberHelpers;

var path = "./RCP_v6_Top.gbr";

var apertures = GerberTools.GerberLines.GetApertureDiameter(path);

var isInches = GerberTools.GerberLines.IsInches(path);

var minLine = GerberTools.GetMinimalDiameter(apertures, isInches);

Console.WriteLine($"Минимальная ширина проводника: {minLine}");

var minContact = GerberTools.GetMinimalDiameter(apertures, isInches, false);

Console.WriteLine($"Минимальная ширина контактной площадки: {minContact}");

var appCodes = GerberTools.GerberLines.GetLinesLength(path);

// Вывод всех длин
appCodes.ParseResult.ForEach(delegate (Dictionary<ApertureKeys, string> item)
{
    Console.WriteLine(item[ApertureKeys.Aperture]);
    Console.WriteLine(item[ApertureKeys.Length]);
    Console.WriteLine(string.Empty);
});

Console.WriteLine($"Минимальная длина проводника: {GerberTools.GetMinimalLength(appCodes, isInches)}");
Console.WriteLine($"Средняя длина проводника: {GerberTools.GetAverageLength(appCodes, isInches)}");
```

## Что умеет

- [x] Парсинг апертур
- [x] Парсинг комманд
- [x] Получение диаметров апертур
- [x] Определение проводников и площадок
- [x] Расчет длин проводников
- [x] Статистические показатели
- [ ] То же самое, но для двусторонней ПП
