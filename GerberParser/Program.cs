﻿using GerberParser.Lib;
using GerberParser.Lib.GerberHelpers;

var path = "./SADCPCB_v2_Top.gbr";

var apertures = GerberTools.GerberLines.GetApertureDiameter(path);

var isInches = GerberTools.GerberLines.IsInches(path);

var minLine = GerberTools.GetMinimalDiameter(apertures, isInches);

Console.WriteLine($"Минимальная ширина проводника: {minLine}");

var minContact = GerberTools.GetMinimalDiameter(apertures, isInches, false);

Console.WriteLine($"Минимальная ширина контактной площадки: {minContact}");

var appCodes = GerberTools.GerberLines.GetLinesLength(path);

// Вывод всех длин
appCodes.ParseResult.ForEach(delegate (Dictionary<ApertureKeys, string> item)
{
    Console.WriteLine(item[ApertureKeys.Aperture]);
    Console.WriteLine(item[ApertureKeys.Length]);
    Console.WriteLine(string.Empty);
});

Console.WriteLine($"Минимальная длина проводника: {GerberTools.GetMinimalLength(appCodes, isInches)}");
Console.WriteLine($"Средняя длина проводника: {GerberTools.GetAverageLength(appCodes, isInches)}");
