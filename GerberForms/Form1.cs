using System.IO;
using System.Windows.Forms.Design;
using GerberParser.Lib;
using GerberParser.Lib.GerberHelpers;

namespace GerberForms;

public partial class Form1 : Form
{
    public Form1()
    {
        InitializeComponent();

        dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        dataGridView2.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

        foreach (DataGridViewColumn column in dataGridView1.Columns)
        {
            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        foreach (DataGridViewColumn column in dataGridView2.Columns)
        {
            column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
    }

    private void button1_Click(object sender, EventArgs e)
    {
        using (OpenFileDialog openFileDialog = new OpenFileDialog())
        {
            openFileDialog.Title = "�������� ����";
            openFileDialog.Filter = "Gerber Files|*.gbr";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string selectedFilePath = openFileDialog.FileName;
                string fileName = Path.GetFileName(selectedFilePath);

                button1.Text = fileName;
                var apertures = GerberTools.GerberLines.GetApertureDiameter(selectedFilePath);
                var appCodes = GerberTools.GerberLines.GetLinesLength(selectedFilePath);

                var isInches = GerberTools.GerberLines.IsInches(selectedFilePath);

                var minLine = GerberTools.GetMinimalDiameter(apertures, isInches);
                var minWidth = GerberTools.GetMinimalLength(appCodes, isInches);
                var minContact = GerberTools.GetMinimalDiameter(apertures, isInches, false);

                var sred = GerberTools.GetAverageLength(appCodes, isInches);

                dataGridView1.Rows.Clear();
                foreach (var aperture in apertures)
                {
                    dataGridView1.Rows.Add(aperture[ApertureKeys.Aperture], aperture[ApertureKeys.Diameter]);
                }
                dataGridView2.Rows.Clear();
                foreach (var appCode in appCodes.ParseResult)
                {
                    dataGridView2.Rows.Add(appCode[ApertureKeys.Aperture], appCode[ApertureKeys.Length]);
                }

                dataGridView1.RowHeadersVisible = false;
                dataGridView2.RowHeadersVisible = false;

                listBox1.Items.Clear();
                listBox1.Items.Add($"����������� ������ ����������: {minLine}");
                listBox1.Items.Add($"����������� ������ ���������� ��������: {minContact}");
                listBox1.Items.Add($"����������� ����� ����������: {minWidth}");
                listBox1.Items.Add($"������� ����� ����������: {sred}");

            }
        }
    }

    private void Form1_Load(object sender, EventArgs e)
    {

    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void dataGridView2_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void dataGridView2_CellContentClick_2(object sender, DataGridViewCellEventArgs e)
    {

    }

    private void label1_Click_1(object sender, EventArgs e)
    {

    }
}