﻿using System.Windows.Forms;

namespace GerberForms;

partial class Form1
{
    private System.ComponentModel.IContainer components = null;

    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }


    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
        button1 = new Button();
        listBox1 = new ListBox();
        dataGridView1 = new DataGridView();
        apertureName = new DataGridViewTextBoxColumn();
        apertureDiametr = new DataGridViewTextBoxColumn();
        dataGridView2 = new DataGridView();
        apertureName2 = new DataGridViewTextBoxColumn();
        apertureLength = new DataGridViewTextBoxColumn();
        label1 = new Label();
        ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
        ((System.ComponentModel.ISupportInitialize)dataGridView2).BeginInit();
        SuspendLayout();
        // 
        // button1
        // 
        button1.Location = new Point(12, 12);
        button1.Name = "button1";
        button1.Size = new Size(120, 38);
        button1.TabIndex = 0;
        button1.Text = "Выбрать файл";
        button1.UseVisualStyleBackColor = true;
        button1.Click += button1_Click;
        // 
        // listBox1
        // 
        listBox1.FormattingEnabled = true;
        listBox1.ItemHeight = 15;
        listBox1.Location = new Point(435, 12);
        listBox1.Name = "listBox1";
        listBox1.Size = new Size(353, 64);
        listBox1.TabIndex = 2;
        listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
        // 
        // dataGridView1
        // 
        dataGridView1.AllowUserToAddRows = false;
        dataGridView1.AllowUserToDeleteRows = false;
        dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        dataGridView1.BackgroundColor = SystemColors.Window;
        dataGridView1.BorderStyle = BorderStyle.Fixed3D;
        dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        dataGridView1.Columns.AddRange(new DataGridViewColumn[] { apertureName, apertureDiametr });
        dataGridView1.Location = new Point(103, 104);
        dataGridView1.Name = "dataGridView1";
        dataGridView1.ReadOnly = true;
        dataGridView1.RowTemplate.Height = 25;
        dataGridView1.Size = new Size(245, 325);
        dataGridView1.TabIndex = 3;
        // 
        // apertureName
        // 
        apertureName.HeaderText = "Имя апертуры";
        apertureName.Name = "apertureName";
        apertureName.ReadOnly = true;
        apertureName.Width = 112;
        // 
        // apertureDiametr
        // 
        apertureDiametr.HeaderText = "Диаметр";
        apertureDiametr.Name = "apertureDiametr";
        apertureDiametr.ReadOnly = true;
        apertureDiametr.Width = 80;
        // 
        // dataGridView2
        // 
        dataGridView2.AllowUserToAddRows = false;
        dataGridView2.AllowUserToDeleteRows = false;
        dataGridView2.Anchor = AnchorStyles.Right;
        dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        dataGridView2.BackgroundColor = SystemColors.Window;
        dataGridView2.BorderStyle = BorderStyle.Fixed3D;
        dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        dataGridView2.Columns.AddRange(new DataGridViewColumn[] { apertureName2, apertureLength });
        dataGridView2.Location = new Point(457, 104);
        dataGridView2.Name = "dataGridView2";
        dataGridView2.ReadOnly = true;
        dataGridView2.RowTemplate.Height = 25;
        dataGridView2.Size = new Size(245, 325);
        dataGridView2.TabIndex = 4;
        dataGridView2.CellContentClick += dataGridView2_CellContentClick_2;
        // 
        // apertureName2
        // 
        apertureName2.HeaderText = "Имя апертуры";
        apertureName2.Name = "apertureName2";
        apertureName2.ReadOnly = true;
        apertureName2.Width = 112;
        // 
        // apertureLength
        // 
        apertureLength.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        apertureLength.HeaderText = "Длина";
        apertureLength.Name = "apertureLength";
        apertureLength.ReadOnly = true;
        apertureLength.Width = 67;
        // 
        // label1
        // 
        label1.AutoSize = true;
        label1.Location = new Point(12, 61);
        label1.Name = "label1";
        label1.Size = new Size(183, 15);
        label1.TabIndex = 5;
        label1.Text = "Все параметры выражены в мм";
        label1.Click += label1_Click_1;
        // 
        // Form1
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        ClientSize = new Size(800, 450);
        Controls.Add(label1);
        Controls.Add(dataGridView2);
        Controls.Add(dataGridView1);
        Controls.Add(listBox1);
        Controls.Add(button1);
        FormBorderStyle = FormBorderStyle.FixedSingle;
        Icon = (Icon)resources.GetObject("$this.Icon");
        MaximizeBox = false;
        Name = "Form1";
        Text = "GerberParser";
        Load += Form1_Load;
        ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
        ((System.ComponentModel.ISupportInitialize)dataGridView2).EndInit();
        ResumeLayout(false);
        PerformLayout();
    }

    private Button button1;
    private ListBox listBox1;
    private DataGridView dataGridView1;
    private DataGridViewTextBoxColumn apertureName;
    private DataGridViewTextBoxColumn apertureDiametr;
    private DataGridView dataGridView2;
    private DataGridViewTextBoxColumn apertureName2;
    private DataGridViewTextBoxColumn apertureLength;
    private Label label1;
}