using GerberParser.Lib.GerberHelpers;

namespace GerberParser.Lib.GerberTypes;

public class GetLinesResult
{
    public required List<Dictionary<ApertureKeys, string>> ParseResult { get; set; }
    public int Count { get; set; }
}