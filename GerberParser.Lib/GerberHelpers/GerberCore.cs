using System.Text.RegularExpressions;

namespace GerberParser.Lib.GerberHelpers;

public partial class GerberCore
{
    public GerberCore() { }

    internal static string? ReadFileByte(string? path)
    {
        try
        {
            if (path == null)
                throw new ArgumentException("Файл не найден");

            return File.ReadAllText(path);
        }
        catch
        {
            return null;
        }
    }

    internal static List<string> GetApertureFromFile(string allCode)
    {

        string pattern = string.Format($@"ADD(.*?)*%");
        Regex rg = new(pattern);

        var apertures = rg.Matches(allCode)
                          .Select(m => m.Value)
                          .ToList();

        return apertures;
    }

    public bool IsInches(string allCode)
    {
        string? codes;
        if (allCode.Contains('/') || allCode.Contains(@"\"[0]))
        {
            codes = ReadFileByte(allCode);
        }
        else
        {
            codes = allCode;
        }

        if (codes == null) return false;
        if (codes.Contains("%MOIN*%"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    internal virtual List<string> GetAperturesCode(List<string> apertures, string allCode)
    {
        string[] apertureTemplate = { "C", "R", "P", "O", "D" };
        List<string> apertureCode = new();

        if (apertures.Count == 1)
        {
            foreach (var aperture in apertures)
            {
                foreach (var template in apertureTemplate)
                {
                    string code = allCode.Split($"{aperture.Split("AD")[1].Split(template)[0]}*")[1]
                        .Split("M02*")[0];
                    code = MyRegex().Replace(code, "");
                    if (code.Length != 0)
                    {
                        apertureCode.Add(code);
                        break;
                    }
                }
            }
        }
        else
        {
            for (var i = 0; i < apertures.Count; i++)
            {
                foreach (var template in apertureTemplate)
                {
                    if (i == apertures.Count - 1)
                    {
                        string code = allCode.Split($"{apertures[i].Split("AD")[1].Split(template)[0]}*")[1]
                            .Split("MO2*")[0];
                        code = MyRegex().Replace(code, "");
                        if (code.Length != 0)
                        {
                            apertureCode.Add(code);
                            break;
                        }
                    }
                    else
                    {
                        string code = allCode.Split($"{apertures[i].Split("AD")[1].Split(template)[0]}*")[1]
                            .Split($"{apertures[i + 1].Split("AD")[1].Split(template)[0]}*")[0];
                        code = MyRegex().Replace(code, "");
                        if (code.Length != 0)
                        {
                            apertureCode.Add(code);
                            break;
                        }
                    }
                }
            }
        }

        return apertureCode;
    }

    [GeneratedRegex("\\t|\\n|\\r")]
    private static partial Regex MyRegex();
}