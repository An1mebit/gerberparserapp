using System.Globalization;
using System.Text.RegularExpressions;
using GerberParser.Lib.GerberTypes;

namespace GerberParser.Lib.GerberHelpers;

public enum ApertureKeys
{
    Aperture,
    Diameter,
    Points,
    Type,
    Length
}

public partial class GerberLines : GerberCore
{
    public GerberLines() { }


    /// <summary>
    /// Рассчитывает все апертуры и их диаметры.
    /// Если диаметр меньше 0,55, то это проводник, иначе контактная площадка
    /// </summary>
    /// <param name="path">Путь до файла</param>
    /// <returns>
    /// Список словарей: aperture - код апертурыб diameter - диаметр апертуры
    /// </returns>
    public List<Dictionary<ApertureKeys, string>> GetApertureDiameter(string path)
    {
        try
        {
            var allCode = ReadFileByte(path);
            var multyplex = 1.0;
            if (IsInches(path)) multyplex = 25.4;

            if (allCode == null)
                return new List<Dictionary<ApertureKeys, string>>();

            var apertures = GetApertureFromFile(allCode);

            var apertureDiameters = new List<Dictionary<ApertureKeys, string>>();
            foreach (var aperture in apertures)
            {
                var trimedNumbers = TrimNumber().Replace(aperture, "");
                if (!IsLineTrim().IsMatch(trimedNumbers) || IsCadGeneratedTrim().IsMatch(trimedNumbers)) continue;

                if (aperture.IndexOf("P", StringComparison.Ordinal) != -1)
                {
                    var diameter = aperture.Split("P,")[1].Split("X")[0];
                    apertureDiameters.Add(new Dictionary<ApertureKeys, string>()
                    {
                        [ApertureKeys.Aperture] = $@"{aperture.Split("AD")[1].Split("P")[0]}",
                        [ApertureKeys.Diameter] = (double.Parse(diameter, CultureInfo.InvariantCulture) * multyplex).ToString().Replace(",", ".")
                    });
                }
                else if (aperture.IndexOf("R", StringComparison.Ordinal) != -1)
                {
                    var diameter = aperture.Split("R,")[1].Split("X")[0];
                    apertureDiameters.Add(new Dictionary<ApertureKeys, string>()
                    {
                        [ApertureKeys.Aperture] = $@"{aperture.Split("AD")[1].Split("R")[0]}",
                        [ApertureKeys.Diameter] = (double.Parse(diameter, CultureInfo.InvariantCulture) * multyplex).ToString().Replace(",", ".")
                    });
                }
                else if (aperture.IndexOf("C", StringComparison.Ordinal) != -1)
                {
                    var diameter = aperture.Split("C,")[1].Split("*")[0];
                    apertureDiameters.Add(new Dictionary<ApertureKeys, string>()
                    {
                        [ApertureKeys.Aperture] = $@"{aperture.Split("AD")[1].Split("C")[0]}",
                        [ApertureKeys.Diameter] = (double.Parse(diameter, CultureInfo.InvariantCulture) * multyplex).ToString().Replace(",", ".")
                    });
                }
            }

            return apertureDiameters;
        }
        catch
        {
            throw new ArgumentException("Непредвиденные индексы апертур");
        }
    }

    /// <summary>
    /// Парсер всех операций D01, D02, D03
    /// </summary>
    /// <param name="path">Путь до файла</param>
    /// <returns>Список всех операций и апертур</returns>
    public List<Dictionary<ApertureKeys, string>> GetAperturesCodeWithName(string path)
    {
        var allCode = ReadFileByte(path);
        if (allCode == null)
            return new List<Dictionary<ApertureKeys, string>>();

        allCode = RemoveSpaces().Replace(allCode, "");
        var rg = TrimReg();
        allCode = allCode.Replace("G54", "");
        var apertureTemplate = rg.Matches(allCode).Select(m => m.Value).ToList();
        apertureTemplate.Add("M02");
        var apertureCodeWithName = new List<Dictionary<ApertureKeys, string>>();
        for (var i = 0; i < apertureTemplate.Count - 1; i++)
        {
            var itemCount = apertureCodeWithName.GroupBy(m => m[ApertureKeys.Aperture]);
            itemCount = itemCount.Where(m => m.Key == apertureTemplate[i]);

            var code = new List<string>();
            if (itemCount.Any())
                code = allCode.Split(apertureTemplate[i])[itemCount.ToList()[0].Count() + 1]
                .Split(apertureTemplate[i + 1])[0]
                .Split("*")
                .Where(m => m != "").ToList();
            else
                code = allCode.Split(apertureTemplate[i])[1]
                    .Split(apertureTemplate[i + 1])[0].Split("*")
                    .Where(m => m != "").ToList();

            if (code[0].Contains("D03"))
            {
                var commandDictionary = new Dictionary<ApertureKeys, string>()
                {
                    [ApertureKeys.Aperture] = $@"{apertureTemplate[i]}",
                    [ApertureKeys.Points] = string.Join(",", code),
                    [ApertureKeys.Type] = "Flash"
                };
                apertureCodeWithName.Add(commandDictionary);
            }
            else
            {
                if (!code[0].Contains("D02"))
                {
                    var line = apertureCodeWithName.Last()[ApertureKeys.Points].Trim('[', ']').Split(",").ToList();

                    code.Insert(0, line.Last().Replace("D03", "D02"));
                }
                var commandDictionary = new Dictionary<ApertureKeys, string>()
                {
                    [ApertureKeys.Aperture] = $@"{apertureTemplate[i]}",
                    [ApertureKeys.Points] = string.Join(",", code),
                    [ApertureKeys.Type] = "Line"
                };
                apertureCodeWithName.Add(commandDictionary);
            }
        }

        return apertureCodeWithName;
    }

    private List<Dictionary<ApertureKeys, string>> SplitLinesByMove(List<Dictionary<ApertureKeys, string>> apertureCode)
    {
        foreach (var apertureInfo in apertureCode.ToList())
        {
            var aperture = apertureInfo[ApertureKeys.Aperture];
            var type = apertureInfo[ApertureKeys.Type];
            var points = apertureInfo[ApertureKeys.Points].Trim('[', ']').Split(",").ToList();

            var moveOperations = points.Where(m => m.IndexOf("D02", StringComparison.Ordinal) != -1).ToList();

            if (moveOperations.Count > 7)
            {
                apertureCode.Remove(apertureInfo);

                for (var i = 0; i < moveOperations.Count; i++)
                {
                    if (i == moveOperations.Count - 1)
                    {
                        var segment = points.Skip(points.FindIndex(m => m == moveOperations[i]))
                            .Take(
                                points.Count -
                                points.FindIndex(m => m == moveOperations[i])
                            ).Select(m => m).ToList();

                        apertureCode.Add(new Dictionary<ApertureKeys, string>()
                        {
                            [ApertureKeys.Aperture] = aperture,
                            [ApertureKeys.Points] = string.Join(',', segment),
                            [ApertureKeys.Type] = type
                        });
                    }
                    else
                    {
                        var segment = points.Skip(points.FindIndex(m => m == moveOperations[i]))
                            .Take(
                                points.FindIndex(m => m == moveOperations[i + 1]) -
                                points.FindIndex(m => m == moveOperations[i])
                            ).Select(m => m).ToList();

                        apertureCode.Add(new Dictionary<ApertureKeys, string>()
                        {
                            [ApertureKeys.Aperture] = aperture,
                            [ApertureKeys.Points] = string.Join(',', segment),
                            [ApertureKeys.Type] = type
                        });
                    }
                }
            }
        }

        return apertureCode;
    }

    private List<Dictionary<ApertureKeys, string>> RemoveHighDiameter(List<Dictionary<ApertureKeys, string>> apertureCode,
                                                                List<Dictionary<ApertureKeys, string>> apertureDiameters, string path)
    {
        var multyplex = 1.0;
        if (IsInches(path))
            multyplex = 25.4;
        var lineDiameter = apertureDiameters.Where(
            m => double.Parse(m[ApertureKeys.Diameter], CultureInfo.InvariantCulture) <= 0.55 * multyplex)
            .MaxBy(m => double.Parse(m[ApertureKeys.Diameter], CultureInfo.InvariantCulture));

        if (lineDiameter == null)
            return new List<Dictionary<ApertureKeys, string>>();

        foreach (var code in apertureCode.ToList())
        {
            var aperture = apertureDiameters.Where(
                m => "*" + m[ApertureKeys.Aperture] == code[ApertureKeys.Aperture]).ToList()[0];

            if (double.Parse(aperture[ApertureKeys.Diameter], CultureInfo.InvariantCulture) >
                double.Parse(lineDiameter[ApertureKeys.Diameter], CultureInfo.InvariantCulture))
                apertureCode.Remove(code);
        }

        return apertureCode;
    }

    /// <summary>
    /// Парсер длин всех линий
    /// </summary>
    /// <param name="path">Путь до файла</param>
    /// <param name="skipZeros">Пропускать нули или нет</param>
    /// <returns>Список объектов GetLinesResult</returns>
    public GetLinesResult GetLinesLength(string path, bool skipZeros = true)
    {
        try
        {
            List<Dictionary<ApertureKeys, string>> apertureLengths = new();
            var allAppertures = GetAperturesCodeWithName(path);
            var diameters = GetApertureDiameter(path);

            allAppertures = allAppertures.Where(m => m[ApertureKeys.Type] != "Flash").ToList();
            allAppertures = SplitLinesByMove(allAppertures);
            allAppertures = RemoveHighDiameter(allAppertures, diameters, path);

            allAppertures.ForEach(delegate (Dictionary<ApertureKeys, string> item)
            {
                var points = item[ApertureKeys.Points].Trim('[', ']').Split(",").ToList();
                var sum = 0.0;
                if (skipZeros && points.Count.Equals(1))
                    return;

                for (var i = 0; i < points.Count - 1; i++)
                {
                    if (points[i].Contains('X') && points[i].Contains('Y')
                    && points[i + 1].Contains('X') && points[i + 1].Contains('Y'))
                    {
                        var x1 = double.Parse(points[i].Split("X")[1].Split("Y")[0], CultureInfo.InvariantCulture);
                        var y1 = double.Parse(points[i].Split("Y")[1].Split("D0")[0], CultureInfo.InvariantCulture);
                        var x2 = double.Parse(points[i + 1].Split("X")[1].Split("Y")[0], CultureInfo.InvariantCulture);
                        var y2 = double.Parse(points[i + 1].Split("Y")[1].Split("D0")[0], CultureInfo.InvariantCulture);

                        sum += Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                    }
                }
                if (skipZeros && sum == 0) return;
                apertureLengths.Add(new Dictionary<ApertureKeys, string>()
                {
                    [ApertureKeys.Aperture] = item[ApertureKeys.Aperture],
                    [ApertureKeys.Length] = $"{sum}"
                });
            });

            var result = new GetLinesResult()
            {
                ParseResult = apertureLengths,
                Count = apertureLengths.Count
            };

            return result;
        }
        catch
        {
            throw new FormatException("Неправильный формат координат");
        }
    }

    [GeneratedRegex("[*/%]D(\\d{2})")]
    private static partial Regex TrimReg();

    [GeneratedRegex("\\t|\\n|\\r")]
    private static partial Regex RemoveSpaces();
    [GeneratedRegex("\\d+")]
    private static partial Regex TrimNumber();
    [GeneratedRegex("ADD(P|R|C)")]
    private static partial Regex IsLineTrim();
    [GeneratedRegex("ADDCadxCAP")]
    private static partial Regex IsCadGeneratedTrim();
}