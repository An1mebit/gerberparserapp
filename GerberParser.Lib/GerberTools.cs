﻿using System.Globalization;
using GerberParser.Lib.GerberHelpers;
using GerberParser.Lib.GerberTypes;

namespace GerberParser.Lib
{
    static public class GerberTools
    {
        static public readonly GerberLines GerberLines = new();

        /// <summary>
        /// Используется при работе с GetApertureDiameter
        /// </summary>
        /// <param name="apertures">Список из апертур и диаметров</param>
        /// <param name="isInches">Обязательное поле и проверка на дюймы</param>
        /// <param name="isBelow">Меньше ли диаметр 0,55</param>
        /// <returns>Минимальное значение диаметра из списка</returns>
        public static double GetMinimalDiameter(List<Dictionary<ApertureKeys, string>> apertures, bool isInches, bool isBelow = true)
        {
            try
            {
                var minLine = 0.0;

                if (isBelow)
                {
                    minLine = apertures.Where(
                        item => item.TryGetValue(ApertureKeys.Diameter, out string? value)
                        && value != null
                        && double.Parse(value, CultureInfo.InvariantCulture) < 0.55).Min(
                        item => double.Parse(item[ApertureKeys.Diameter], CultureInfo.InvariantCulture)
                    );
                }
                else
                {
                    minLine = apertures.Where(
                        item => item.TryGetValue(ApertureKeys.Diameter, out string? value)
                        && value != null
                        && double.Parse(value, CultureInfo.InvariantCulture) > 0.55).Min(
                        item => double.Parse(item[ApertureKeys.Diameter], CultureInfo.InvariantCulture)
                    );
                }

                return minLine;
            }
            catch
            {
                return 0.0;
            }
        }

        /// <summary>
        /// Если список ParseResult в объекте пустой, то вернет 0
        /// </summary>
        /// <param name="linesResult">Объект из GetLinesLength</param>
        /// <param name="isInches">Обязательное поле и проверка на дюймы</param>
        /// <returns>Минимальная длина проводника</returns>
        public static double GetMinimalLength(GetLinesResult linesResult, bool isInches)
        {
            try
            {
                var multyplex = 1.0;
                if (isInches)
                    multyplex = 25.4;
                if (linesResult.ParseResult.All(item => item.ContainsKey(ApertureKeys.Length)))
                {
                    return linesResult.ParseResult.Min(item => double.Parse(item[ApertureKeys.Length].Replace(',', '.'), CultureInfo.InvariantCulture)) * multyplex;
                }

                return 0.0;
            }
            catch
            {
                return 0.0;
            }
        }

        /// <summary>
        /// Если список ParseResult в объекте пустой, то вернет 0
        /// </summary>
        /// <param name="linesResult">Объект из GetLinesLength</param>
        /// <param name="isInches">Обязательное поле и проверка на дюймы</param>
        /// <returns>Средняя длина проводника</returns>
        public static double GetAverageLength(GetLinesResult linesResult, bool isInches)
        {
            try
            {
                var multyplex = 1.0;
                if (isInches)
                    multyplex = 25.4;
                if (linesResult.ParseResult.All(item => item.ContainsKey(ApertureKeys.Length)))
                {
                    return linesResult.ParseResult.Average(item => double.Parse(item[ApertureKeys.Length].Replace(',', '.'), CultureInfo.InvariantCulture)) * multyplex;
                }

                return 0.0;
            }
            catch
            {
                return 0.0;
            }
        }
    }
}